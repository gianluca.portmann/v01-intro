import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;

class PersonenPanel extends JPanel {

	private Person person1;
	private Person person2;

	/**
	 * <pre>
	 * - NullLayout setzen - person1 mit Mozart erzeugen - person2 mit Beethoven
	 * erzeugen - person1 bei Koordinaten (15, 0, 215, 350) hinzuf�gen - person2
	 * bei Koordinaten (230, 0, 215, 350) hinzuf�gen
	 */
	public PersonenPanel() {
		setLayout(null);
		person1 = new Person("---", "Mario", "Mario.png", "mario.wav");
		person2 = new Person("---", "Link", "Link.jpg", "zelda.wav");
		
		add(person1);
		add(person2);
		
		person1.setBounds(15, 0, 215, 350);
		person2.setBounds(230, 0, 215, 350);

	}

}

public class PersonenPanelFrame extends JFrame {
	private PersonenPanel view;

	public PersonenPanelFrame() {
		view = new PersonenPanel();
		add(view);
		setSize(460, 400);
		setTitle("Personen Panel");
		setIconImage(Utility.loadResourceImage("clef.png"));
		setResizable(true);
		setVisible(true);
		setLocationRelativeTo(null);
	}

	public static void main(String args[]) {
		PersonenPanelFrame frame = new PersonenPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
}